# Django Restful Upload

Current version: 1.0.1

django-upload-media is a Django & rest_framework application:

* 上传文件的 RESTFUL 接口;
* 支持 json ＋ base64 格式的上传;
