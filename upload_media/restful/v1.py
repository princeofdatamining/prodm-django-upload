from rest_framework import validators, permissions, status
from rest_framework.generics import *
from rest_framework.permissions import *
from rest_framework.serializers import *
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from qc_utils.rest_framework.views import *
from qc_utils.rest_framework.permissions import *
from qc_utils.rest_framework.serializers import *
from qc_utils.rest_framework.fields import *

from .. import models, constant

class UploadSerializer(ModelSerializer):

    user = PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = models.Upload
        fields = ('media',
            'create_time', 'user', 'category',
            'filename', 'filesize', 'checksum', 'width', 'height')

class UploadSubmitSerializer(ModelSerializer):

    USER_FIELDS = 'user_id'
    base64 = Base64FileField(source='media')

    class Meta:
        model = models.Upload
        fields = ('media', 'base64',)

    # 既支持 "multipart/form-data" 又支持 "application/json"
    def to_internal_value(self, data):
        media, base64 = self.fields['media'], self.fields['base64']
        if data.get('base64'):
            media.run_validation = raise_skip
        else:
            base64.raise_skip = True
        return super(UploadSubmitSerializer, self).to_internal_value(data)

class UploadsView(ListCreateAPIView):

    queryset = models.Uploads.none()
    submit_serializer_class = UploadSubmitSerializer
    serializer_class = UploadSerializer

#

from django.conf.urls import url, include
urlpatterns = [
    url(r'^$', UploadsView.as_view(), name='upload_media'),
]

from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = format_suffix_patterns(urlpatterns)
