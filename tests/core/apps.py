from django.apps import AppConfig, apps

class CoreAppConfig(AppConfig):

    name = 'core'

    def ready(self):
        try:
            self.build_data()
        except:
            pass

    def build_data(self):
        from django.contrib.auth.models import User
        root, created = User.objects.get_or_create(defaults=dict(
            is_superuser=True, is_staff=True, is_active=True,
        ), username='root')
        if created:
            root.set_password('password')
            root.save()
        #
